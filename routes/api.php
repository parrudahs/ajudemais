<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::resource('user', 'UserController');
Route::resource('medalha', 'MedalhaController');
Route::resource('instituicao', 'InstituicaoController');

Route::post('apoiar-instituicao', 'UserController@apoiarInstituicao');
Route::post('para-apoiar-instituicao', 'UserController@paraDeApoiarInstituicao');

Route::post('login', 'UserController@getLogin');

Route::post('ganhar-medalha', 'MedalhaController@ganharMedalha');
