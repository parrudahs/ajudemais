<?php

namespace App\Http\Controllers;

use App\Entities\UserInstituicao;
use App\Repositories\UsersRepository;
use App\Validators\UserValidator;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use \Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;

class UserController extends Controller
{

    private $repository;
    private $validator;

    /**
     * UserController constructor.
     * @param UsersRepository $repository
     * @param UserValidator $validator
     */
    public function __construct(UsersRepository $repository, UserValidator $validator)
    {
            $this->repository = $repository;
            $this->validator  = $validator;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->repository->all();
    }


    /**
     * @param Request $request
     * @return array|mixed
     */
    public function store(Request $request)
    {
        try{
            $this->validator->with($request->all())->passesOrFail();
            return $this->repository->create($request->all());
        }catch(ValidatorException $e) {

            return [
                'error' => true,
                'message' =>  $e->getMessageBag()
            ];

        }
    }

    /**
     * @param $id
     * @return array|mixed
     */
    public function show($id)
    {
        try {
            return $this->repository->with(['instituicoes','medalhas'])->find($id);
        } catch (ModelNotFoundException $e) {
            return ['error'=>true, 'msg' => 'Usuario não encontrado.'];
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return array|mixed
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail();
            return $this->repository->update($request->all(), $id);
        } catch (ModelNotFoundException $e) {
            return ['error'=>true, 'msg' => 'Usuario não encontrado.'];
        } catch (ValidatorException $e) {
            return [
                'error' => true,
                'message' =>  $e->getMessageBag()
            ];
        }
    }

    /**
     * @param $id
     * @return array
     */
    public function destroy($id)
    {
        try {
            $this->repository->find($id)->delete();
            return ['success'=>true, 'msg' => 'Usuario foi deletado com sucesso!'];
        } catch (QueryException $e) {
            return ['error'=>true, 'msg' => 'Usuario não pode ser apagado pois existe um ou mais clientes vinculados a ele.'];
        } catch (ModelNotFoundException $e) {
            return ['error'=>true, 'msg' => 'Usuario não encontrado.'];
        } catch (\Exception $e) {
            return ['error'=>true, 'msg' => 'Ocorreu algum erro ao excluir o Usuario.'];
        }
    }

    /**
     * @param Request $request
     * @return array|static
     */
    public function apoiarInstituicao(Request $request)
    {
        $ui = new UserInstituicao();
        try{
            foreach ($ui->where('user_id', $request->input('user_id'))->get() as $u) {
                if($u['instituicao_id'] == $request->input('instituicao_id'))
                    return ['error' => true, 'message' => 'você já está apoiando esta instituição'];

            }
            return $ui->create($request->all());
        } catch (QueryException $e) {
            return [
                'error' => true,
                'message' =>  'Ocoreu um erro interno'
            ];
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function paraDeApoiarInstituicao(Request $request)
    {
        $ui = new UserInstituicao();
        $a = false;
        try{
            foreach ($ui->where('user_id', $request->input('user_id'))->get() as $u) {
                if($u['instituicao_id'] == $request->input('instituicao_id'))         {
                    $ui->find($u['id'])->delete();
                    $a = true;
                }
            }
            if($a) {
                return [
                    'error' => false,
                    'message' => 'você parou de apoiar a instituição'
                ];
            }else {
                return [
                    'error' => true,
                    'message' => 'você não apoia a instituição'
                ];
            }
        } catch (QueryException $e) {
            return [
                'error' => true,
                'message' =>  'Ocoreu um erro interno'
            ];
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getLogin(Request $request)
    {
        if(Auth::attempt(['email' => $request['email'], 'password' => $request['password']])) {
            return [
                'success' => true,
                'message' => 'seja bem vindo'
            ];
        }
        return [
            'success' => false,
            'message' => 'login ou senha invalida'
        ];
    }

}
