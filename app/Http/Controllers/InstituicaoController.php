<?php

namespace App\Http\Controllers;

use App\Repositories\InstituicaoRepository;
use App\Validators\InstituicaoValidator;
use Illuminate\Http\Request;

use \Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;

class InstituicaoController extends Controller
{

    private $repository;
    private $validator;

    /**
     * InstituicaoController constructor.
     * @param InstituicaoRepository $repository
     * @param InstituicaoValidator $validator
     */
    public function __construct(InstituicaoRepository $repository, InstituicaoValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->repository->all();
    }


    /**
     * @param Request $request
     * @return array|mixed
     */
    public function store(Request $request)
    {
        try{

            $this->validator->with($request->all())->passesOrFail();
            return $this->repository->create($request->all());

        }catch(ValidatorException $e) {

            return [
                'error' => true,
                'message' =>  $e->getMessageBag()
            ];

        }
    }


    /**
     * @param $id
     * @return array|mixed
     */
    public function show($id)
    {
        try {
            return $this->repository->find($id);
        } catch (ModelNotFoundException $e) {
            return ['error'=>true, 'msg' => 'medalha não encontrado.'];
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return array|mixed
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail();
            return $this->repository->update($request->all(), $id);
        } catch (ModelNotFoundException $e) {
            return ['error'=>true, 'msg' => 'medalha não encontrado.'];
        } catch (ValidatorException $e) {
            return [
                'error' => true,
                'message' =>  $e->getMessageBag()
            ];
        }
    }

    /**
     * @param $id
     * @return array
     */
    public function destroy($id)
    {
        try {
            $this->repository->find($id)->delete();
            return ['success'=>true, 'msg' => 'medalha foi deletado com sucesso!'];
        } catch (QueryException $e) {
            return ['error'=>true, 'msg' => 'medalha não pode ser apagado pois existe um ou mais clientes vinculados a ele.'];
        } catch (ModelNotFoundException $e) {
            return ['error'=>true, 'msg' => 'medalha não encontrado.'];
        } catch (\Exception $e) {
            return ['error'=>true, 'msg' => 'Ocorreu algum erro ao excluir o medalha.'];
        }
    }

    
}
