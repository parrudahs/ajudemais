<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MedalhaRepository
 * @package namespace App\Repositories;
 */
interface MedalhaRepository extends RepositoryInterface
{
    //
}
