<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Medalhas extends Model
{
    protected $fillable = [
        'nome', 'imagem', 'descricao'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_medalhas', 'medalha_id', 'user_id');
    }
}
