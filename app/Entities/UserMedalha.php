<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class UserMedalha extends Model
{
    protected $table = 'user_medalhas';

    protected $fillable = [
        'user_id', 'medalha_id'
    ];
}
