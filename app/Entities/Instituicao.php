<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Instituicao extends Model
{

    protected $table = 'instituicoes';

    protected $fillable = [
        'nome','descricao', 'categoria', 'imagem', 'telefone', 'email','rua', 'bairro',
        'numero', 'cidade', 'estado', 'pais'
    ];

    public function apoiando()
    {
        return $this->belongsToMany(User::class, 'user_instituicoes', 'instituicao_id', 'user_id');
    }
    
}
