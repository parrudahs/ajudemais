<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Amigos extends Model
{
    protected $table = 'amigos';

    protected $fillable = [
        'amigo_id', 'user_id'
    ];
}
