<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome', 'email', 'password', 'imagem'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function instituicoes()
    {
        return $this->belongsToMany(Instituicao::class, 'user_instituicoes', 'user_id', 'instituicao_id');
    }

    public function medalhas()
    {
        return $this->belongsToMany(Medalhas::class, 'user_medalhas', 'user_id', 'medalha_id');
    }

    public function amigos()
    {
        return $this->belongsToMany(Amigos::class, 'amigos', 'user_id', 'amigo_id');
    }
}
