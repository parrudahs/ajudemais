<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class UserInstituicao extends Model
{
    protected $table = 'user_instituicoes';

    protected $fillable = [
        'user_id', 'instituicao_id'
    ];
}
