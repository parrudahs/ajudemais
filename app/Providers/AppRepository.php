<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppRepository extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \App\Repositories\UsersRepository::class,
            \App\Repositories\UsersRepositoryEloquent::class
        );

        $this->app->bind(
            \App\Repositories\MedalhaRepository::class,
            \App\Repositories\MedalhaRepositoryEloquent::class
        );

        $this->app->bind(
            \App\Repositories\InstituicaoRepository::class,
            \App\Repositories\InstituicaoRepositoryEloquent::class
        );
    }
}
